package io.gitlab.agluszak.phonebook;

import io.gitlab.agluszak.phonebook.controller.CommandController;
import io.gitlab.agluszak.phonebook.controller.Controller;
import io.gitlab.agluszak.phonebook.service.IPersonStore;
import io.gitlab.agluszak.phonebook.service.MapPersonStore;
import io.gitlab.agluszak.phonebook.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        View view = new View();
        IPersonStore store = new MapPersonStore();
        Scanner scanner = new Scanner(System.in);
        CommandController commandController = new CommandController();
        Controller controller = new Controller(store, view, scanner, commandController);

        controller.mainLoop();
    }

}
