package io.gitlab.agluszak.phonebook.commands;

import io.gitlab.agluszak.phonebook.model.Person;
import io.gitlab.agluszak.phonebook.service.IPersonStore;

public class AddCommand implements UndoableCommand {
    private final int id;
    private final Person person;
    private final IPersonStore store;

    public AddCommand(IPersonStore store, int id, Person person) {
        this.id = id;
        this.person = person;
        this.store = store;
    }

    @Override
    public void execute() {
        store.addPerson(id, person);
    }

    @Override
    public void undo() {
        store.deletePerson(id);
    }
}
