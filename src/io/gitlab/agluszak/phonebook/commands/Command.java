package io.gitlab.agluszak.phonebook.commands;


// Command design pattern
public interface Command {
    void execute();
}
