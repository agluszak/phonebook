package io.gitlab.agluszak.phonebook.commands;

import io.gitlab.agluszak.phonebook.model.Person;
import io.gitlab.agluszak.phonebook.service.IPersonStore;
import io.gitlab.agluszak.phonebook.view.View;

import java.util.Map;

public class ListCommand implements Command {
    private final IPersonStore store;
    private final View view;

    public ListCommand(IPersonStore store, View view) {
        this.store = store;
        this.view = view;
    }

    @Override
    public void execute() {
        Map<Integer, Person> people = store.listPeople();
        view.listPeople(people);
    }
}
