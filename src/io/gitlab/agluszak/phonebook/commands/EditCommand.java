package io.gitlab.agluszak.phonebook.commands;

import io.gitlab.agluszak.phonebook.model.Person;
import io.gitlab.agluszak.phonebook.service.IPersonStore;

public class EditCommand implements UndoableCommand {
    private final int id;
    private final Person oldData;
    private final Person newData;
    private final IPersonStore store;

    public EditCommand(IPersonStore store, int id, Person newData) {
        this.id = id;
        this.oldData = store.getPerson(id);
        this.newData = newData;
        this.store = store;
    }
    @Override
    public void execute() {
        store.editPerson(id, newData);
    }

    @Override
    public void undo() {
        store.editPerson(id, oldData);
    }
}
