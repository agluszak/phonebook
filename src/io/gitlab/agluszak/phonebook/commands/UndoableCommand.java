package io.gitlab.agluszak.phonebook.commands;

// Command which is able to reverse its effects
public interface UndoableCommand extends Command {
    void undo();
}
