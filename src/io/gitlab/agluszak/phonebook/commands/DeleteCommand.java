package io.gitlab.agluszak.phonebook.commands;

import io.gitlab.agluszak.phonebook.model.Person;
import io.gitlab.agluszak.phonebook.service.IPersonStore;

public class DeleteCommand implements UndoableCommand {
    private final int id;
    private final Person deletedPerson;
    private final IPersonStore store;

    public DeleteCommand(IPersonStore store, int id) {
        this.id = id;
        this.store = store;
        this.deletedPerson = store.getPerson(id);
    }

    @Override
    public void execute() {
        store.deletePerson(id);
    }

    @Override
    public void undo() {
        store.addPerson(id, deletedPerson);
    }
}
