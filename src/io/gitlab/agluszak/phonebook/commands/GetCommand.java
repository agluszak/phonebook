package io.gitlab.agluszak.phonebook.commands;

import io.gitlab.agluszak.phonebook.service.IPersonStore;
import io.gitlab.agluszak.phonebook.view.View;

public class GetCommand implements Command {
    private final int id;
    private final IPersonStore store;
    private final View view;

    public GetCommand(IPersonStore store, int id, View view) {
        this.store = store;
        this.id = id;
        this.view = view;
    }

    @Override
    public void execute() {
        view.printPerson(store.getPerson(id));
    }
}
