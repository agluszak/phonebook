package io.gitlab.agluszak.phonebook.service;

import io.gitlab.agluszak.phonebook.model.Person;

import java.util.Map;
import java.util.TreeMap;

public class MapPersonStore implements IPersonStore{
    final private Map<Integer, Person> store;

    public MapPersonStore() {
        store = new TreeMap<>();
    }

    @Override
    public void addPerson(int id, Person person) {
        store.put(id, person);
    }

    @Override
    public void editPerson(int id, Person newData) {
        store.put(id, newData);
    }

    @Override
    public void deletePerson(int id) {
        store.remove(id);
    }

    @Override
    public boolean existsPerson(int id) {
        return store.containsKey(id);
    }

    @Override
    public Map<Integer, Person> listPeople() {
        Map<Integer, Person> map = new TreeMap<>();
        store.forEach(map::put);
        return map;
    }

    @Override
    public Person getPerson(int id) {
        return store.get(id);
    }
}
