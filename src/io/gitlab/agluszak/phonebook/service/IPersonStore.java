package io.gitlab.agluszak.phonebook.service;

import io.gitlab.agluszak.phonebook.model.Person;

import java.util.Map;

public interface IPersonStore {
    void addPerson(int id, Person person);

    void editPerson(int id, Person newData);

    Map<Integer, Person> listPeople();

    Person getPerson(int id);

    void deletePerson(int id);

    boolean existsPerson(int id);
}
