package io.gitlab.agluszak.phonebook.controller;

import io.gitlab.agluszak.phonebook.commands.Command;
import io.gitlab.agluszak.phonebook.commands.UndoableCommand;

import java.util.Stack;

// We keep the history of undoable commands in a LIFO stack.
// When we undo a command, we put it in a 'futureHistory' stack in case we want to redo it.
// 'futureHistory' is cleared when we execute a new command.
public class CommandController {
    private final Stack<UndoableCommand> history;
    private final Stack<UndoableCommand> futureHistory;

    public CommandController() {
        this.history = new Stack<>();
        this.futureHistory = new Stack<>();
    }

    public void executeCommand(Command command) {
        command.execute();
    }

    public void executeUndoableCommand(UndoableCommand command) {
        command.execute();
        history.push(command);
        futureHistory.removeAllElements();
    }

    public void undo() {
        if (!history.empty()) {
            UndoableCommand commandToUndo = history.pop();
            commandToUndo.undo();
            futureHistory.push(commandToUndo);
        }
    }

    public void redo() {
        if (!futureHistory.empty()) {
            UndoableCommand commandToRedo = futureHistory.pop();
            commandToRedo.execute();
            history.push(commandToRedo);
        }
    }

    public boolean canUndo() {
        return !history.empty();
    }

    public boolean canRedo() {
        return !futureHistory.empty();
    }
}
