package io.gitlab.agluszak.phonebook.controller;

import io.gitlab.agluszak.phonebook.commands.*;
import io.gitlab.agluszak.phonebook.model.Person;
import io.gitlab.agluszak.phonebook.service.IPersonStore;
import io.gitlab.agluszak.phonebook.view.View;

import java.util.Scanner;

public class Controller {
    private final IPersonStore store;
    private final View view;
    private final Scanner scanner;
    private final CommandController commandController;

    public Controller(IPersonStore store,
                      View view,
                      Scanner scanner,
                      CommandController commandController) {
        this.store = store;
        this.view = view;
        this.scanner = scanner;
        this.commandController = commandController;
    }

    private void handleAddPerson() {
        view.inputId();
        int id;
        try {
            id = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            view.invalidId();
            return;
        }
        if (store.existsPerson(id)) {
            view.idExists();
        } else {
            view.inputName();
            String name = scanner.nextLine();
            view.inputSurname();
            String surname = scanner.nextLine();
            view.inputNumber();
            String number = scanner.nextLine();
            Person person = new Person(name, surname, number);
            UndoableCommand command = new AddCommand(store, id, person);
            commandController.executeUndoableCommand(command);
            view.ok();
        }
    }

    private void handleDeletePerson() {
        view.inputId();
        int id;
        try {
            id = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            view.invalidId();
            return;
        }
        if (store.existsPerson(id)) {
            UndoableCommand command = new DeleteCommand(store, id);
            commandController.executeUndoableCommand(command);
            view.ok();
        } else {
            view.idDoesNotExit();
        }

    }

    private void handleEditPerson() {
        view.inputId();
        int id;
        try {
            id = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            view.invalidId();
            return;
        }
        if (store.existsPerson(id)) {
            view.inputName();
            String name = scanner.nextLine();
            view.inputSurname();
            String surname = scanner.nextLine();
            view.inputNumber();
            String number = scanner.nextLine();
            Person person = new Person(name, surname, number);
            UndoableCommand command = new EditCommand(store, id, person);
            commandController.executeUndoableCommand(command);
            view.ok();
        } else {
            view.idDoesNotExit();
        }
    }

    private void handleListPeople() {
        Command command = new ListCommand(store, view);
        commandController.executeCommand(command);
    }

    private void handleGetPerson() {
        view.inputId();
        int id;
        try {
            id = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            view.invalidId();
            return;
        }
        Command command = new GetCommand(store, id, view);
        commandController.executeCommand(command);
    }

    private void handleUndo() {
        if (commandController.canUndo()) {
            commandController.undo();
            view.ok();
        } else {
            view.cannotUndo();
        }
    }

    private void handleRedo() {
        if (commandController.canRedo()) {
            commandController.redo();
            view.ok();
        } else {
            view.cannotRedo();
        }
    }

    public void mainLoop() {
        while (scanner.hasNextLine()) {
            String input = scanner.nextLine();
            switch (input) {
                case "addPerson":
                    handleAddPerson();
                    break;
                case "deletePerson":
                    handleDeletePerson();
                    break;
                case "editPerson":
                    handleEditPerson();
                    break;
                case "listPeople":
                    handleListPeople();
                    break;
                case "getPerson":
                    handleGetPerson();
                    break;
                case "undo":
                    handleUndo();
                    break;
                case "redo":
                    handleRedo();
                    break;
                case "exit":
                    return;
                default:
                    view.wrongCommand();
                    break;
            }
        }
    }
}
