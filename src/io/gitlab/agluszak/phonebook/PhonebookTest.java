package io.gitlab.agluszak.phonebook;

import io.gitlab.agluszak.phonebook.controller.CommandController;
import io.gitlab.agluszak.phonebook.controller.Controller;
import io.gitlab.agluszak.phonebook.service.IPersonStore;
import io.gitlab.agluszak.phonebook.service.MapPersonStore;
import io.gitlab.agluszak.phonebook.view.View;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.Assert.*;

public class PhonebookTest {

    IPersonStore testHelper(String inputCommands) {
        View view = new View();
        IPersonStore store = new MapPersonStore();

        InputStream testStream = new ByteArrayInputStream(inputCommands.getBytes());

        Scanner scanner = new Scanner(testStream);
        CommandController commandController = new CommandController();
        Controller controller = new Controller(store, view, scanner, commandController);

        controller.mainLoop();

        try {
            testStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return store;
    }

    @Test
    public void addPersonTest() {
        StringBuilder sb = new StringBuilder();

        sb.append("addPerson\n");
        sb.append("1\n");
        sb.append("Roman\n");
        sb.append("Dzik\n");
        sb.append("123456789\n");

        IPersonStore store = testHelper(sb.toString());
        assertTrue(store.existsPerson(1));
        assertEquals("Roman", store.getPerson(1).getName());
        assertEquals("Dzik", store.getPerson(1).getSurname());
        assertEquals("123456789", store.getPerson(1).getNumber());
    }

    @Test
    public void deletePersonTest() {
        StringBuilder sb = new StringBuilder();

        sb.append("addPerson\n");
        sb.append("1\n");
        sb.append("Roman\n");
        sb.append("Dzik\n");
        sb.append("123456789\n");
        sb.append("deletePerson\n");
        sb.append("1\n");

        IPersonStore store = testHelper(sb.toString());
        assertFalse(store.existsPerson(1));
    }

    @Test
    public void undoTest() {
        StringBuilder sb = new StringBuilder();

        sb.append("addPerson\n");
        sb.append("1\n");
        sb.append("Roman\n");
        sb.append("Dzik\n");
        sb.append("123456789\n");
        sb.append("deletePerson\n");
        sb.append("1\n");
        sb.append("undo\n");

        IPersonStore store = testHelper(sb.toString());
        assertTrue(store.existsPerson(1));
        assertEquals("Roman", store.getPerson(1).getName());
        assertEquals("Dzik", store.getPerson(1).getSurname());
        assertEquals("123456789", store.getPerson(1).getNumber());
    }

    @Test
    public void redoTest() {
        StringBuilder sb = new StringBuilder();

        sb.append("addPerson\n");
        sb.append("1\n");
        sb.append("Roman\n");
        sb.append("Dzik\n");
        sb.append("123456789\n");
        sb.append("deletePerson\n");
        sb.append("1\n");
        sb.append("undo\n");
        sb.append("redo\n");

        IPersonStore store = testHelper(sb.toString());
        assertFalse(store.existsPerson(1));
    }

    @Test
    public void invalidInputTest() {
        IPersonStore store = testHelper("asdasdasd");
    }

    @Test
    public void invalidId() {
        StringBuilder sb = new StringBuilder();

        sb.append("addPerson\n");
        sb.append("a1\n");
        IPersonStore store = testHelper(sb.toString());
        assertFalse(store.existsPerson(1));
    }

}