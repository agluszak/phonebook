package io.gitlab.agluszak.phonebook.view;

import io.gitlab.agluszak.phonebook.model.Person;

import java.util.Map;

public class View {
    public void wrongCommand() {
        System.out.println("Wrong command.");
    }

    public void listPeople(Map<Integer, Person> people) {
        if (people.isEmpty()) {
            System.out.println("There are no people.");
        } else {
            System.out.println(String.format("%4s|%15s|%15s|%15s", "ID", "NAME", "SURNAME", "NUMBER"));
            people.forEach((id, person) -> {
                    String formatted = String.format("%4d|%15s|%15s|%15s",
                            id, person.getName(), person.getSurname(), person.getNumber());
                    System.out.println(formatted);
            });
        }
    }

    public void printPerson(Person person) {
        System.out.println(person.toString());
    }

    public void idExists() {
        System.out.println("Person with such ID already exists.");
    }

    public void idDoesNotExit() {
        System.out.println("Person with such ID does not exist.");
    }

    public void inputId() {
        System.out.println("Please input an ID.");
    }

    public void inputName() {
        System.out.println("Please input a name.");
    }

    public void inputSurname() {
        System.out.println("Please input a surname.");
    }

    public void inputNumber() {
        System.out.println("Please input a number.");
    }

    public void cannotUndo() {
        System.out.println("There's nothing to undo!");
    }

    public void cannotRedo() {
        System.out.println("There's nothing to redo!");
    }

    public void ok() {
        System.out.println("Done.");
    }

    public void invalidId() {
        System.out.println("ID is invalid.");
    }
}
