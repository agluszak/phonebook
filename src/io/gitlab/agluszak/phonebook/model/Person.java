package io.gitlab.agluszak.phonebook.model;

public class Person {
    private final String name;
    private final String surname;
    private final String number;

    public Person(String name, String surname, String number) {
        this.name = name;
        this.surname = surname;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return this.name + " " + this.surname + " " + this.number;
    }
}
